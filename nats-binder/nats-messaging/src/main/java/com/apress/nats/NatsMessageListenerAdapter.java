package com.apress.nats;

import io.nats.client.Dispatcher;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class NatsMessageListenerAdapter {
    private final static String NO_CONNECTION = "Can not subscribe to {0} because the connection to JetStream is null.";
    private final static String NO_DISPATCHER = "The dispatcher is null, this adapter isn't subscribed to anything.";
    private final static String NO_SUBJECT = "Can not subscribe to a null or empty subject.";
    private final static String NO_ADAPTOR = "The NATS message listener is null.";
    private final JetStreamConnection jetStreamConnection;
    private final String subject;
    private final NatsMessageListener adapter;
    private Dispatcher dispatcher;

    public void start(){
        if(jetStreamConnection == null || jetStreamConnection.getConnection() == null){
            log.error(NO_CONNECTION, subject);
            return;
        }
        if(subject == null){
            log.error(NO_SUBJECT);
            return;
        }
        if(adapter == null){
            log.error(NO_ADAPTOR);
            return;
        }
        log.debug("Creating Message Listener for the subject: {}", subject);
        dispatcher = this.jetStreamConnection.getConnection().createDispatcher((msg) -> {});
        dispatcher.subscribe(this.subject, (msg) -> adapter.onMessage(msg.getData()));
        log.debug("Subscribed to: {}", subject);
    }

    public void stop(){
        if(dispatcher == null){
            log.error(NO_DISPATCHER);
            return;
        }
        if(subject == null){
            log.error(NO_SUBJECT);
            return;
        }
        log.debug("Unsubscribing from: {}", subject);
        dispatcher.unsubscribe(subject,300);
    }
}
