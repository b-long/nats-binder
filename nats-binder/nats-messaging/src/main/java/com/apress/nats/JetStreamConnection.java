package com.apress.nats;

import io.nats.client.Connection;
import io.nats.client.Nats;
import io.nats.client.JetStream;
import io.nats.client.JetStreamOptions;
import lombok.Data;

import java.io.IOException;

@Data
public class JetStreamConnection {
    private Connection connection;
    private JetStream jetStream;
    private NatsProperties natsProperties;

    private JetStreamConnection(){}

    public JetStreamConnection(NatsProperties natsProperties) throws IOException, InterruptedException {
        this.natsProperties = natsProperties;
        this.connection = Nats.connect("nats://" + natsProperties.getHost() + ":" + natsProperties.getPort());

        //is this doing anything?
        JetStreamOptions jsOptions = JetStreamOptions.builder()
                                                     .prefix(natsProperties.getStreams().stream().findFirst().get())
                                                     .publishNoAck(false)
                                                     .build();
        this.jetStream = connection.jetStream();
    }

}
