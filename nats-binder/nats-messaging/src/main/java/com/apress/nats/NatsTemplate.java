package com.apress.nats;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.util.SerializationUtils;

import java.nio.charset.StandardCharsets;

@Slf4j
@AllArgsConstructor
@Data
public class NatsTemplate {
    private static final String NULL_CONNECTION = "The connection to NATS jet stream is null, can not publish subject: {0}, message: {1}";
    private static final String NULL_SUBJECT = "Can not publish the message: {0} because the subject is null or empty.";
    private static final String NULL_MESSAGE = "Can not publish a null or empty message for the subject: {0}";
    private static final String SENDING = "Sending: {0}-{1}";
    private JetStreamConnection jetStreamConnection;

    public void send(String subject, String message){
        if(checkAndLog(subject, message)){
            return;
        }
        this.jetStreamConnection.getConnection().publish(subject, message.getBytes(StandardCharsets.UTF_8));
    }

    public void send(String subject, Message<?> message){
        if(checkAndLog(subject, message == null ? null : message.toString())){
            return;
        }
        this.jetStreamConnection.getConnection().publish(subject, SerializationUtils.serialize(message));
    }

    private boolean checkAndLog(String subject, String message){
        if(jetStreamConnection.getConnection() == null){
            log.error(String.format(NULL_CONNECTION, subject, message));
            return true;
        }
        if(subject == null || subject.isEmpty()){
            log.warn(String.format(NULL_SUBJECT, message));
            return true;
        }
        if(message == null || message.isEmpty()){
            log.warn(String.format(NULL_MESSAGE, subject));
            return true;
        }

        log.debug(String.format(SENDING), subject, message);
        return false;
    }

}
