package com.apress.nats;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@EnableConfigurationProperties(NatsProperties.class)
@Configuration
public class NatsConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public JetStreamConnection natsConnection(NatsProperties natsProperties) throws IOException, InterruptedException {
        return new JetStreamConnection(natsProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    public NatsTemplate natsTemplate(JetStreamConnection jetStreamConnection){
        return new NatsTemplate(jetStreamConnection);
    }

}
